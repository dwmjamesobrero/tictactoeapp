import React from 'react';
import Cell from './Cell';

export default (props) => (
  <div className="board">
    {
      props.board.map((symbol, index) => (
        <Cell key={index} index={index} symbol={symbol} turn={props.turn} />
      ))
    }
  </div>
);

