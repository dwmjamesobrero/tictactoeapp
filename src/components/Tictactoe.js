import React from 'react';
import  { connect } from 'react-redux';
import Board from './Board';
import Settings from './Settings';
import Result from './Result';
import { setSettings } from '../actions/tictactoe';
import PlayStatus from './PlayStatus';

class Tictactoe extends React.Component {
  render() {
    const {board, turn, winner, started, player1Symbol, player1, player2, gameHistory} = this.props.tictactoe;
    let gameResult = '';
    let playerTurnName = '';

    if (['x', 'o'].includes(winner.toLowerCase())) {
      if (winner === player1Symbol) {
        gameResult = player1 + ' win the game!';
      } else {
        gameResult = player2 + ' win the game!';
      }
    } else if (winner.toLowerCase() === 'd') {
      gameResult = 'Draw game!';
    }

    if (player1Symbol === turn) {
      playerTurnName = player1;
    } else {
      playerTurnName = player2;
    }

    return (
      <div className="container">
        <div className="row pt-5">
          <div className="col-4" />
          <div className="col-4">
            { !started &&
              <Settings
                tictactoe={this.props.tictactoe}
                onSubmit={(settings) => {
                  this.props.dispatch(setSettings(settings));
                }}
              />
            }
          </div>
          <div className="col-4" />
        </div>
        { started &&
          <div className="row">
            <div className="col-6">
              <div className="h-25">
                <PlayStatus
                  playerTurnName={playerTurnName}
                  turnSymbol={turn}
                  winner={winner}
                  gameResult={gameResult}
                />
              </div>
              <div>
                <Result
                  gameHistory={gameHistory}
                />
              </div>
            </div>
            <div className="col-6">
              <Board
                board={board}
                turn={turn}
                winner ={winner}
              />
            </div>
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    tictactoe: state.tictactoe
  };
}

export default connect(mapStateToProps)(Tictactoe);