import React from 'react';
import { rematch, surrender } from '../actions/tictactoe';
import { connect } from 'react-redux';
import GameModal from './GameModal';

class PlayStatus extends React.Component {
  constructor(props) {
    super(props);
  }

  handleRematch = () => {
    console.log('here');
    this.props.dispatch(rematch())
  }

  handleSurrender = () => {
    this.props.dispatch(surrender())
  }


  render() {
    return (
      <div>
        <div>
          <h3>Player Turn: <span className="badge badge-info">{this.props.playerTurnName}</span></h3>
          <h3>Symbol: <span className="badge badge-info">{this.props.turnSymbol.toUpperCase()}</span></h3>
        </div>

        <GameModal
          gameResult={this.props.gameResult}
          handleRematch={this.handleRematch}
          handleSurrender={this.handleSurrender}
        />
      </div>
    )
  }
}

export default connect()(PlayStatus);