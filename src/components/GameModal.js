import React from 'react';
import Modal from 'react-modal';

export default class GameModal extends React.Component {
  componentWillMount() {
    Modal.setAppElement('body');
  }

  render() {
    return (
      <Modal
        isOpen={!!this.props.gameResult}
        contentLabel="Result"
        className="game-modal"
      >
        <h3 className="game-modal__title">GAME OVER</h3>
        { this.props.gameResult &&
          <div>
            <h1 className="mb-5">{this.props.gameResult}</h1>
            <button className="btn btn-success" onClick={this.props.handleRematch}>Rematch</button>
            <button className="btn btn-danger ml-2" onClick={this.props.handleSurrender}>Surrender</button>
          </div>
        }
      </Modal>
    )
  }
}