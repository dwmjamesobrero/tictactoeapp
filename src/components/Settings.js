import React from 'react';

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      turn: props.tictactoe ? props.tictactoe.turn : 'x',
      player1: props.tictactoe ? props.tictactoe.player1 : 'player1',
      player2: props.tictactoe ? props.tictactoe.player2 : 'player2',
      error: ''
    };
  }

  onSelectChange = (e) => {
    const turn = e.target.value;
    this.setState(() => ({ turn }))
  }

  onPlayer1NameChange = (e) => {
    const player1 = e.target.value;
    this.setState(() => ({ player1 }));
  }

  onPlayer2NameChange = (e) => {
    const player2 = e.target.value;
    this.setState(() => ({ player2 }));
  }

  onSubmit = (e) => {
    e.preventDefault();

    if (!this.state.player1 || !this.state.player2) {
      this.setState(() => ({ error: "Player name is required!" }))
    } else if(this.state.player1 === this.state.player2) {
      this.setState(() => ({ error: "Player name should not be the same!" }))
    } else {
      this.setState(() => ({ error: '' }));
      this.props.onSubmit({
        turn: this.state.turn,
        player1: this.state.player1,
        player1Symbol: this.state.turn,
        player2: this.state.player2,
        started: true
      })
    }
  }

  render() {
    return (
      <div className="player-option">
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Player 1: </label>
            <input
              className="form-control"
              type="text"
              value={this.state.player1}
              onChange={this.onPlayer1NameChange}
            />
          </div>
          <div className="form-group">
            <label>Symbol :</label>
            <select
              className="form-control"
              value={this.state.turn}
              onChange={this.onSelectChange}
            >
              <option value="x">X</option>
              <option value="o">O</option>
            </select>
          </div>
          <div className="form-group">
            <label>Player 2 :</label>
            <input
              className="form-control"
              type="text"
              value={this.state.player2}
              onChange={this.onPlayer2NameChange}
            />
          </div>

          <button className="btn btn-success">Start Game</button>
        </form>
        {this.state.error && <p className="error">{this.state.error}</p>}
      </div>
    )
  }
}
