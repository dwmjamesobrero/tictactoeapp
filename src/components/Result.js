import React from 'react';
import ResultList from './ResultList';

export default (props) => {
  return (
    <div>
      <hr/>
      <h3>Game History :</h3>
      <table className="table">
        <thead className="thead-light">
          <tr>
            <th scope="col">Game</th>
            <th scope="col">Result</th>
            <th scope="col">Moves</th>
          </tr>
        </thead>
        <tbody>
          { !!!props.gameHistory.length && <tr><td align="center" colSpan="3">No game result....</td></tr>}
          {
            props.gameHistory.map((history, index) =>(
              <ResultList key={index} {...history} />
            ))
          }
        </tbody>
      </table>
    </div>
  )
}

