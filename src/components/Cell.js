import React from 'react';
import { connect } from 'react-redux';
import { setSymbol } from '../actions/tictactoe';

const Cell = ({dispatch, index, symbol, turn}) => (
  <div
    className="cell"
    onClick={(e) => {
      dispatch(setSymbol({ index, symbol: turn }));
    }}
  >
    <i>{symbol.toUpperCase()}</i>
  </div>
)

export default connect()(Cell);