import React from 'react';

export default ({game, winner, symbol, moveCount}) =>  (
  <tr>
    <td scope="row">{game}</td>
    <td>{ winner ? `${winner} wins` : 'Draw'}</td>
    <td>{ winner ? `${moveCount} moves (${symbol})` : 'N/A'}</td>
  </tr>
)