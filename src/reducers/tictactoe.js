const initialState = {
  board: [
    '', '', '',
    '', '', '',
    '', '', ''
  ],
  player1: 'James',
  player1Symbol: 'x',
  player2: 'Challenger',
  turn: 'x',
  winner: 'n',
  started: false,
  game: 1,
  gameHistory: []
};

const checkWinner = (board) => {
  const check = (a,b,c) => {
      return !!(a + b + c).match(/^(xxx|ooo)$/gi);
  };

  // vertical match
  if (check(board[0], board[1], board[2])) return board[0];
  if (check(board[3], board[4], board[5])) return board[3];
  if (check(board[6], board[7], board[8])) return board[6];

  // horizontal match
  if (check(board[0], board[3], board[6])) return board[0];
  if (check(board[1], board[4], board[7])) return board[1];
  if (check(board[2], board[5], board[8])) return board[2];

  // diagonal match
  if (check(board[0], board[4], board[8])) return board[0];
  if (check(board[2], board[4], board[6])) return board[2];

  if (board.join('').length === 9) return 'd';
  return 'n';
};


export default (state = initialState, action) => {
  switch(action.type) {
    case "SET_SYMBOL":
      if (!state.board[action.index] && state.winner === 'n') {
        const newBoard = state.board;
        newBoard[action.index] = action.symbol;

        let result = state.gameHistory;
        const winner = checkWinner(newBoard);

        if (winner !== 'n') {
          // Draw game
          if (winner === 'd') {
            result = [
              ...result,
              {
                game: state.game,
                winner: '',
                symbol: '',
                moveCount: ''
              }
            ]
          } else {
            const moveCount = state.board.filter((move) => move === winner).length;

            result = [
              ...result,
              {
                game: state.game,
                winner: winner === state.player1Symbol ? state.player1 : state.player2,
                symbol: winner,
                moveCount: moveCount
              }
            ]
          }
        }

        return {
          ...state,
          board: newBoard,
          turn: action.symbol === 'o' ? 'x' : 'o',
          winner: winner,
          gameHistory: result
        };
      }
      return state;
    case "SET_SETTINGS":
      return {
        ...state,
        turn: action.turn,
        player1: action.player1,
        player1Symbol: action.player1Symbol,
        player2: action.player2,
        started: action.started
      }
    case "REMATCH":
      return {
        ...state,
        board: state.board.map(() => ''),
        winner: 'n',
        turn: state.player1Symbol,
        game: state.game + 1
      };
    case "SURRENDER":
      return {
        ...state,
        winner: 'n',
        board: state.board.map(() => ''),
        started: false,
        turn: 'x',
        gameHistory:[]
      }
    default:
      return state;
  }
}