import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from  './store/store';
import Tictactoe from './components/Tictactoe';
// import 'normalize.css/normalize.css';
import './styles/styles.scss';

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <Tictactoe />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));
