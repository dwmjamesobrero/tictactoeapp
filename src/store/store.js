import {createStore, combineReducers} from 'redux';
import tictactoeReducer from '../reducers/tictactoe';

export default () => {
  const store = createStore(
    combineReducers({
      tictactoe: tictactoeReducer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );

  return store;
}