export const setSymbol = ({index, symbol}) => ({
  type: 'SET_SYMBOL',
  index,
  symbol
});

export const rematch = () => ({
  type: 'REMATCH'
});

export const setSettings = ({player1, player2, turn, started, player1Symbol}) => ({
  type: 'SET_SETTINGS',
  player1,
  player1Symbol,
  player2,
  turn,
  started
});

export const surrender = () => ({
  type: 'SURRENDER'
});